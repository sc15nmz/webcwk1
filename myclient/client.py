import requests, json, sys, getpass

login_menu = """
Available commands:
login <url>
news [-id="<value>"] [-cat="<value>"] [-reg="<value>"] [-date="<value>"]
list
exit
"""

main_menu = """
Available commands:
post
news [-id="<value>"] [-cat="<value>"] [-reg="<value>"] [-date="<value>"]
list
delete <story_key>
logout
"""

logged_in = False


def login(s, url):
    # Get user input
    username = input("Username: ")
    password = getpass.getpass("Password: ")

    data = {
        "username": username,
        "password": password
    }
    url += "/api/login/"

    headers = {'Content-type': 'application/x-www-form-urlencoded'}
    response = s.post(url, data=data, headers=headers)

    # Check if successful
    if response.status_code == 200:
        print("Hello, ", username)

        return int(response.status_code)

    print(response.status_code, ". Unable to log in at the time")


def logout(s, url):
    url += "/api/logout/"

    response = s.post(url)

    # Check if successful
    if response.status_code == 200:
        print("Logout successful. Bye!")

        return int(response.status_code)

    print(response.status_code, ". Unable to log out at the time")


def post_story(s, url):
    # Used to validate input
    invalid_input = True

    # For all 4 fields, get input and validate it
    headline = input("Headline: ")
    while invalid_input:
        if len(headline) > 64:
            print("Headline must be less than 64 characters. Please try again")
            headline = input("Headline: ")
        else:
            invalid_input = False

    invalid_input = True
    category = input("Category: ")
    while invalid_input:
        if category.upper() not in ["POL", "TECH", "ART", "TRIVIA"]:
            print("Category must be POL, TECH, ART or TRIVIA. Please try again")
            category = input("Category: ")
        else:
            invalid_input = False

    invalid_input = True
    region = input("Region: ")
    while invalid_input:
        if region.upper() not in ["UK", "EU", "W"]:
            print("Region must be UK, EU or W. Please try again")
            region = input("Region: ")
        else:
            invalid_input = False

    invalid_input = True
    details = input("Details: ")
    while invalid_input:
        if len(details) > 64:
            print("Details must be less than 512 characters. Please try again")
            details = input("Details: ")
        else:
            invalid_input = False

    # Prepare the url and data
    url += "/api/poststory/"
    data = {
        "headline": headline,
        "details": details,
        "category": category,
        "region": region
    }
    headers = {'Content-type': 'application/json'}
    response = s.post(url, data=json.dumps(data), headers=headers)

    # Print response
    if response.status_code == 201:
        print("Story posted.")


def get_stories(s, arguments):
    # Check if correct number of arguments is sent
    if len(arguments) > 4:
        print("The command accepts at most 4 arguments")
        return

    # Get arguments, split them and extract the values
    filters = []
    filter_values = []
    for argument in arguments:
        option, value = argument.split("=")
        separated_option_values = option.split("-")
        filters.append(separated_option_values[1])

        separated_filter_values = value.split("\"")
        filter_values.append(separated_filter_values[1])

    # Default to * if no argument is sent
    service_id, category, region, date = "*", "*", "*", "*"

    # Override above with user values, if passed
    if "id" in filters:
        service_id = filter_values[filters.index("id")]
    if "cat" in filters:
        category = filter_values[filters.index("cat")]
    if "reg" in filters:
        region = filter_values[filters.index("reg")]
    if "date" in filters:
        date = filter_values[filters.index("date")]

    all_agencies_url = "http://directory.pythonanywhere.com/api/list/"
    if s is not None:
        all_agencies = s.get(all_agencies_url)
    else:
        all_agencies = requests.get(all_agencies_url)

    # Fetch received data
    request_body_unicode = all_agencies.content.decode("utf-8")

    if request_body_unicode:
        request_body = json.loads(request_body_unicode)
    else:
        request_body = []

    agency_list = request_body["agency_list"]
    agencies_urls = []

    # Get all urls that requests will be sent to based on user citeria
    if service_id is not "*":
        for agency in agency_list:
            if agency["agency_code"] == service_id:
                # Make sure the url ends without a / as we sent a GET request
                wrong_url = agency["url"]

                if (wrong_url[-1] == "/"):
                    wrong_url = wrong_url[:-1]

                correct_url = wrong_url
                correct_url += "/api/getstories/"
                agencies_urls.append(correct_url)
    else:
        for agency in agency_list:
            # Make sure all urls end without a / as we sent a GET request
            wrong_url = agency["url"]

            if (wrong_url[-1] == "/"):
                wrong_url = wrong_url[:-1]

            correct_url = wrong_url
            correct_url += "/api/getstories/"
            agencies_urls.append(correct_url)

    # agencies_urls.append("http://127.0.0.1:8000/api/getstories/")

    # Array to store the stories in
    all_stories = []

    # Go through all urls from above and fetch their stories
    for single_url in agencies_urls:
        data = {
            "story_cat": category,
            "story_region": region,
            "story_date": date
        }

        headers = {'Content-type': 'application/json'}
        if s is not None:
            response = s.get(single_url, data=json.dumps(data), headers=headers)
        else:
            response = requests.get(single_url, data=json.dumps(data), headers=headers)
        if (response.status_code == 200):

            request_body_unicode = response.content.decode("utf-8")

            if request_body_unicode:
                request_body = json.loads(request_body_unicode)
            else:
                request_body = []

            all_stories.append(request_body)

    # Print all stories
    for story in all_stories:
        if 'stories' in story:
            stories_json = story['stories']
            for single_story in stories_json:
                if "key" in single_story and "headline" in single_story and "story_cat" in single_story and "story_region" in single_story and "author" in single_story and "story_date" in single_story and "story_details" in single_story:
                    print("\nKey:", single_story["key"])
                    print("Headline:", single_story["headline"])
                    print("Category:", single_story["story_cat"])
                    print("Region:", single_story["story_region"])
                    print("Author:", single_story["author"])
                    print("Date:", single_story["story_date"])
                    print("Details:", single_story["story_details"])
                else:
                    print("\nSome fields seem to be missing. Story displayed as JSON")
                    print(single_story)

def list_services(s = None):
    all_agencies_url = "http://directory.pythonanywhere.com/api/list/"
    if s is not None:
        response = s.get(all_agencies_url)
    else:
        response = requests.get(all_agencies_url)

    # Fetch received data
    request_body_unicode = response.content.decode("utf-8")

    if request_body_unicode:
        request_body = json.loads(request_body_unicode)
    else:
        request_body = []

    agency_list = request_body["agency_list"]

    # Print all agencies
    print("Available Agencies:\n")
    for agency in agency_list:
        print(agency["agency_code"])
        print(agency["agency_name"])
        print(agency["url"])
        print("\n")


def delete_story(s, url, arguments):
    # Check if correct number of arguments is sent
    if len(arguments) != 1:
        print("The command accepts only one arguments (the story to be deleted)")
        return

    url += "/api/deletestory/"

    data = {
        "story_key": arguments[0]
    }
    headers = {'Content-type': 'application/json'}
    response = s.post(url, data=json.dumps(data), headers=headers)

    if response.status_code == 201:
        print("Story deleted.")

        return int(response.status_code)

    print(response.status_code, ". Unable to delete story at the time")


while True:
    # Print base menu
    print(login_menu)

    # Get user input
    option = input("Choose option: ")
    user_input = option.split(" ")
    command = user_input[0]

    if command == "login":
        if len(user_input) == 2:
            url = user_input[1]
        else:
            print("Wrong number of arguments")
            sys.exit(0)
        # Log in the user with a session
        s = requests.session()
        status_code = login(s, url)

        if status_code == 200:
            logged_in = True

            # Until the user logs out
            while logged_in:
                # Print the main menu
                print(main_menu)

                # Get user input
                option = input("Choose option: ")
                arguments = option.split(" ")

                # Simulate switch statement
                if arguments[0] == "post":
                    post_story(s, url)
                elif arguments[0] == "news":
                    get_stories(s, arguments[1:])
                elif arguments[0] == "list":
                    list_services(s)
                elif arguments[0] == "delete":
                    delete_story(s, url, arguments[1:])
                elif arguments[0] == "logout":
                    logout_response = logout(s, url)

                    if logout_response == 200:
                        logged_in = False
                else:
                    print("Command not found")
        else:
            print("Login unsuccessful. Please try again!")
    elif command == "news":
        get_stories(None, user_input[1:])
    elif command == "list":
        list_services(None)
    elif command == "exit":
        sys.exit(0)
    else:
        print("Command not found")
