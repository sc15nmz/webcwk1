from django.utils import timezone
from .models import Author, Article
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse


import json
import datetime


@csrf_exempt
def user_login(request):
    response = HttpResponse()

    # Check if request is POST
    if request.method == 'POST':
        # Check if correct data is sent
        if "username" in request.POST and "password" in request.POST:
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
        else:
            response.status_code = 400
            response["Content-Type"] = "text/plain"
            response.content = "Please supply username and password"

            return response
    else:
        response.status_code = 400
        response["Content-Type"] = "text/plain"
        response.content = "Requires a POST request"

        return response

    if user is not None:
        if user.is_active:
            # Log in the user and sent a response
            login(request, user)

            if (user.is_authenticated):
                print(user.username + ' successfully logged in')

            response.status_code = 200
            response["Content-Type"] = "text/plain"
            response.content = "Welcome " + user.username

            return response
        else:
            response.status_code = 401
            response["Content-Type"] = "text/plain"
            response.content = "Account Disabled"

            return response
    else:
        response.status_code = 401
        response["Content-Type"] = "text/plain"
        response.content = "Invalid Login Details"

        return response


@csrf_exempt
def user_logout(request):
    response = HttpResponse()

    # Check if request is POST
    if request.method == 'POST':
        if (request.user.is_authenticated):
            # Log out the user and sent a response
            logout(request)
            response.status_code = 200
            response["Content-Type"] = "text/plain"
            response.content = "Goodbye!"

            return response
        else:
            response.status_code = 401
            response["Content-Type"] = "text/plain"
            response.content = "User not logged in"

            return response
    else:
        response.status_code = 400
        response["Content-Type"] = "text/plain"
        response.content = "Requires a POST request"

        return response


@csrf_exempt
def post_story(request):
    response = HttpResponse()

    # Check if request is POST and user is authenticated
    if request.method == 'POST':
        if request.user.is_authenticated:
            # Fetch sent data
            request_body_unicode = request.body.decode("utf-8")

            if request_body_unicode:
                request_body = json.loads(request_body_unicode)
            else:
                request_body = []

            # Prepare a new post and save it in the database
            headline = request_body["headline"]
            details = request_body["details"]
            category = request_body["category"]
            region = request_body["region"]
            author = Author.objects.get(username=request.user)

            new_post = Article(author=author, headline=headline, category=category.upper(),
                               region=region.upper(), details=details, date=timezone.now())
            new_post.save()

            response = HttpResponse()
            response.status_code = 201

            return response
        else:
            response.status_code = 503
            response["Content-Type"] = "text/plain"
            response.content = "User not logged in"

            return response
    else:
        response.status_code = 400
        response["Content-Type"] = "text/plain"
        response.content = "Requires a POST request"

        return response


@csrf_exempt
def get_stories(request):
    # Check if request is GET
    if request.method == 'GET':
        # Fetch sent data
        request_body_unicode = request.body.decode("utf-8")

        if request_body_unicode:
            request_body = json.loads(request_body_unicode)
        else:
            request_body = []

        # Parse the passed data so it's * if it's not sent
        if 'story_cat' not in request_body:
            category = '*'
        else:
            category = request_body["story_cat"]
            if category is not '*':
                category = category.upper()

        if 'story_region' not in request_body:
            region = '*'
        else:
            region = request_body["story_region"]
            if region is not '*':
                region = region.upper()

        if 'story_date' not in request_body:
            date = '*'
        else:
            date = request_body["story_date"]
            # Check if user hasn't sent '*' so we can safely cast to an int
            if date is not "*":
                date_parts = date.split("/")
                day = int(date_parts[0])
                month = int(date_parts[1])
                year = int(date_parts[2])
                date = datetime.datetime(year, month, day)

        stories = Article.objects.all()
        # Filter the stories by the user categories
        if category is not '*':
            stories = stories.filter(category=category)

        if region is not '*':
            stories = stories.filter(region=region)

        if date is not '*':
            stories = stories.filter(date__gte=date)

        # If no stories match the filter
        if stories.count() == 0:
            print(category, region, date)
            response = HttpResponse()
            response.status_code = 404
            response["Content-Type"] = "text/plain"
            response.content = "No stories were found"

            return response
        else:
            # Return the stories that match the criteria
            data = []
            for story in stories:
                single_story = {"key":story.id, "headline":story.headline, "story_cat":story.category, "story_region":story.region, "author": story.author.name, "story_date": str(story.date), "story_details": story.details}
                data.append(single_story)

            json_payload = {"stories":data}
            response = HttpResponse(json.dumps(json_payload))
            response.status_code = 200
            response['Content-Type'] = "application/json"

            return response
    else:
        response = HttpResponse()
        response.status_code = 400
        response["Content-Type"] = "text/plain"
        response.content = "Requires a POST request"

        return response


@csrf_exempt
def delete_story(request):
    response = HttpResponse()

    # Check if request is POST
    if request.method == 'POST':
        if (request.user.is_authenticated):
            # Fetch sent data
            request_body_unicode = request.body.decode("utf-8")

            if request_body_unicode:
                request_body = json.loads(request_body_unicode)
            else:
                request_body = []

            if 'story_key' not in request_body:
                return HttpResponse("Story key required")
            else:
                story_key = request_body["story_key"]

            # Find the correct story and delete it
            story = Article.objects.all()
            story = story.filter(pk=story_key)
            story.delete()

            response.status_code = 201

            return response
        else:
            response.status_code = 503
            response["Content-Type"] = "text/plain"
            response.content = "User not logged in"

            return response
    else:
        response.status_code = 500
        response["Content-Type"] = "text/plain"
        response.content = "Requires a POST request"

        return response
