from django.db import models
from django.contrib.auth.models import User

CATEGORIES = (
    ('POL', 'Politics'),
    ('ART', 'Art'),
    ('TECH', 'Technology'),
    ('TRIVIA', 'Trivial')
)

REGIONS = (
    ('UK', 'United Kingdom News'),
    ('EU', 'European News'),
    ('W', 'World News')
)

class Author(models.Model):
	name = models.CharField('Author Name', max_length = 120)
	username = models.ForeignKey(User, on_delete = models.CASCADE)

	def __str__(self):
		return self.name

class Article(models.Model):
	headline = models.CharField('Headline', max_length = 64)
	category = models.CharField('Category', choices = CATEGORIES, max_length = 6)
	region = models.CharField('Region', choices = REGIONS, max_length = 2)
	author = models.ForeignKey(Author, on_delete = models.CASCADE)
	date = models.DateTimeField('Date')
	details = models.CharField('Details', max_length = 512)

	def __str__(self):
		return self.headline + ' was published on ' + self.date.strftime('%d/%m/%Y') + '. Region = ' + self.region + ', category = ' + self.category
