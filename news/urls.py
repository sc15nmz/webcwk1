from django.urls import path, include
from . import views

urlpatterns = [
	path('api/getstories/', views.get_stories, name='get_stories'),
	path('api/login/', views.user_login, name='user_login'),
	path('api/logout/', views.user_logout, name='user_logout'),
	path('api/poststory/', views.post_story, name='post_story'),
	path('api/deletestory/', views.delete_story, name='delete_story'),
]
